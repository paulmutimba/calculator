//Paul Mutimba
//Calculator

#include<conio.h>
#include<iostream>

using namespace std;

float Add(float num1, float num2)
{
	return (num1 + num2);
}

float Subtract(float num1, float num2)
{
	return (num1 - num2);
}

float Multiply(float num1, float num2)
{
	return (num1 * num2);
}

bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0)
	{
		return false;
	}
	else
	{
		answer = (num1 / num2);
		return true;
	}
}

float Pow(float num1, int num2) //recursive function
{
	float exponent = 1;
	if (num1 >= 0 && num2 == 0)
	{
		return 1; //if any number is to power zero, it should return 1;
	}
	else
	{

		int count;
		for (count = 0; count < num2; count++) { //loops over the second number which acts as the the exponent.

			exponent = exponent * num1;
		}
	}
	return exponent;
}

int main()
{

	string more;
	char sign;
	float num1, num2, answer;

	do {

		cout << "Enter +, -, *, /, ^: \n";
		cin >> sign;

		cout << "Enter First Number: \n";
		cin >> num1;
		cout << "Enter Second Number: \n";
		cin >> num2;

		if (num1 > 0 && num2 >= 0)
		{
			switch (sign)
			{
			case '+':
				cout << "Total is: " << Add(num1, num2) << endl;
				break;

			case '-':
				cout << "Difference is: " << Subtract(num1, num2) << endl;
				break;

			case '*':
				cout << "Product is: " << Multiply(num1, num2) << endl;
				break;

			case '/':
				if (Divide(num1, num2, answer)) 
				{
					cout << "Quotient is: " << answer << endl;
				}
				else
				{
					cout << "Cannot divide by zero." << endl;
				}
				
				break;
			case '^':
			{
				cout << "Result is: " << Pow(num1, num2) << endl;
				break;
			}
			default:
				// If the operator is other than +, -, * or /, error message is shown
				cout << "Error! operator is not correct or you have entered a negative number";
				break;
			}
		}
		else {
			cout << "Enter a positive number!";
			break;
		}

		cout << "Calculate another one? (y/n) \n";
		cin >> more;
	} while (more == "y");

	(void)_getch();
	return 0;
}